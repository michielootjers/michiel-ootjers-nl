---
enable: true
title: "What projects stand out"
description: "Don't just take my word for it - Let the projects below speak for themselves"

# Testimonials
testimonials:
  - name: "Zaaksysteem.nl"
    designation: "Lead Developer"
    avatar: "/images/avatar-bi.png"
    content: |
      What started as a Perl monolith is now a microservice architecture using Python and ReactJS. By building a CQRS framework using FastAPI on top and several python workers listening on a AMQP messagebus, I've managed to create a system in which we could rapidly move away from our monolith to a scalable SaaS Application.

      **Techstack**: Python, FastAPI, Pydantic, ReactJS, Redux, S3, Aurora, Docker, Gitlab, Helm, RabbitMQ, Redis, Elasticsearch

  - name: "xxllnc"
    designation: "CTO"
    avatar: "/images/avatar-bi.png"
    content: |
      With over 12 acquisitions, we needed a way to integrate our different software. By creating the **xxllnc Cloud** where all applications are launched and implemented using Platform Engineering, we managed to phase out datacenters and bring our teams together on our multi-organization Kubernetes cluster on AWS. And implemented a cost saving of >40% in the process.

      **Techstack**: AWS Controltower, EKS, Grafana, Loki, Mimir, Prometheus, Terraform, Gitlab pipelines, IAC, KEDA

  - name: "Zaaksysteem.nl"
    designation: "Cloud Engineer"
    avatar: "/images/avatar-bi.png"
    content: |
      We needed the infrastructure to support the new microservice architecture of the software, so we created a Kubernetes cluster with a LGTM stack. Using IaC and infrastructure tests, live changes to the infra were possible. Using metrics we could scale our infrastructure based on customer usage and using Spot Instances we kept our cloud costs maintainable.

      **Techstack**: K8S, HPA, Terraform, Terratest, LGTM, ElasticSearch, ElastiCache, Amazon MQ, Amazon Aurora, S3, Cloudfront

  - name: "Zaaksysteem.nl"
    designation: "Owner"
    avatar: "/images/avatar-bi.png"
    content: "As an owner of a company which grew from 2 to 35 employees, you get used to tackle the occasional challenges. By implementing governance using Holacracy, I've found a way to match our company to our \"open source\"-minded employees when we grew over 20 employees. Coworkers would describe me as a technical people manager, best known for my lead-by-example way of managing. I was formally known as the CTO, but was also responsible for Tech, HR and Finance."

  - name: "MHASMO"
    designation: "Linux Endboss"
    avatar: "/images/avatar-bi.png"
    content: "MHASMO was specialized in creating high-performance infrastructure for customers demanding more than a handful of servers. When Linux was involved, they turned to me to build their infrastructure, which was mostly a set of HAProxy loadbalancers, CentOS/Ubuntu application servers and a DRBD backed master-master MySQL cluster. Rollout using Chef or Puppet. And occasionally advising software teams how to make their software scale on these kind of clusters."

  - name: "KPN HotSpots"
    designation: "Cluster Engineer"
    avatar: "/images/avatar-bi.png"
    content: |
      KPN turned to me when they got in trouble with their provisioning system behind their public hotspots. In less then 9 months we transformed their on Windows based server platform to a stable linux based cluster. By implementing best practices in their software, to make sure the database was friendly queried and sessions were properly stored HotSpots was stable and ready to grow to the future.

      **Techstack**: PHP, CentOS, MSSQL,

# don't create a separate page
_build:
  render: "never"
---
