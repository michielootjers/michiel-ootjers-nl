---
title: Michiel Ootjers
email: michiel@ootjers.nl
image: "/images/about_michiel_ootjers.png"
description: Cloud Native Expert
social:
  - name: gitlab
    icon: fa-brands fa-github
    link: https://gitlab.com/michielootjers

  - name: linkedin
    icon: fa-brands fa-linkedin
    link: https://linkedin.com/in/michielootjers
---
