---
title: "Holacracy: Be a Ferrari"
meta_title: ""
description: "Using Holacracy to improve employee output"
date: 2024-03-27T12:00:00
image: "/images/blog/holacratic_ferrari.jpeg"
categories: ["Entreprenerdship"]
author: "Michiel Ootjers"
tags: ["holacracy", "governance"]
draft: false
---

Be a Ferrari, how I kept my employees engaged after I almost gave up and wanted
to sell my company.

It was 2017, the year we (my business partner Peter and I) almost sold the
company. That's because I realised that I was already seven years in, our
company was successful, but still realised that I was still working more than 60
hours a week with no prospect of having an exit in the near future. Or well...
"exit"..that wasn't even a term we knew right then: we just started this company
as an experiment and had never thought to get this far.

It was after signing away my house for the second time with the bank for a loan,
that I thought to myself: this really needs to stop. I'm not using my coding
skills, I'm not using my creativity and I really don't feel that I'm doing what I
love. It was clear, I wanted that "exit", as soon as possible. When Peter found
out that even he wasn't able to change my mind, he initiated our "Article 5": we are
either both in, or we are both out. Together we decided to start the process of
selling the company. And yeah, the end result, in short, was a process in which
we learned a lot, including that we didn't like the biddings we received.

Bummer...that was a tough realisation, and it let to a conversation in which we
decided that we couldn't continue chasing for the end game while neglecting our
wishes or even our families. So after a period of feeling sorry for ourselves
and doing what we wanted (I bought a Tesla Model S, and Peter finally bought his
new house), we found peace with this situation and started to think how we could
grow the company in a way we both would be happy.

So, in the next year we decided I should find some time to code again, and we
both should find a way to improve the company in a way that others value it the
same as we did. Starting by preparing ourselves for the growth yet to come.
Our group had grown to more than 15 people, a set of people you could
easily describe as a bunch of stubborn but venturous entreprenerds. Or better
said: they were just like Peter and me. We knew this group would grow to 30
people within the year, so it was time to think about our organization
structure. An organizational structure that would match with what Peter and I
would want if we were "just employees" of the company.

#### Holacracy to the rescue

We needed something that would match our organically grown company culture. A
company in which we tend to "leave the power to the people", and give them
enough space to suggest and implement ideas. Or as we knew from our SCRUM
implementation: find the solution of a problem as close as possible to the one
working on it. And no obvious processes, because our employees didn't like the
idea of "growing to a corporate", which would sound familiar for most startup
owners. And although we knew that Peter and I had some essential experience for
the company, we also believed that most of the important work got done by our
coworkers. Oh, and to be honest:  we personally didn't like hierarchy very much,
so it needed to be something flat. We needed something different.

So when I was hearing a friend of mine talking about a governance system called
Holacracy, it piqued my interest. A system that focusses on the work what needs
to be done in your company, and allows you to group those work specific tasks
into roles. And although it has some sort of hierarchy, that hierarchy is based
on these roles, and not around the person. Hell, you could even forget about
calling people managers if you want. For example, you could have two roles: one
being the financial endboss (Better than manager right? ;)), and one being the
one organizing social events in the company. Both will have different positions
in the hierarchy: I might be the most important person in the room regarding
operational finances, but on the other side I am also taking orders from our
SET-endboss (Social Events Team).

But the most interesting part about Holacracy for us technology enthusiasts was:
it was a live governance system within an app. It had a solution for the fact
that nothing stays the same in a scale-up. At one moment you are hiring a
designer for your application, and one month later you find out he is a master
in marketing and you need him to transition into that role. You can imagine how
that would work out in a classical hierarchy: you set goals for his yearly
performance report, only to find out that his goal "Create a new customer
template in CSS" was entirely irrelevant in the end.

We decided to look into it, and after one presentation, Peter and I were sold.
This allowed us to give our coworkers all the space they needed to grow our
product to new heights, without the corporate feeling, but with the necessary
processes to allow for a sustainable future. So, after some preparation, we
started our journey with a lot of fun, laughter and new stories to remember.


#### Be a Ferrari

After the introduction of holacracy to our coworkers, and after we all signed a
contract as a symbolic start, our journey together started with one bold phrase:
"Be a ferrari". In Holacracy It means that when you are in a role, you can do
anything you want within the rules of Holacracy to reach that roles purpose. You
can imagine all the jokes and laughter. From HR: "I can just use 5 million euros
of the company to throw the biggest party imaginable to keep morale up", or "I
will install a unlimited amount of holidays for everyone". Yeah....with great
power comes great responsibility. So we set a rule for the financial endboss
that required everyone to consult him whenever the budget exceeded a certain
amount. So yes, we implemented processes, which were the "rules of the game".

Within weeks we got to learn the tricks of the trade: we got into "Tacticals"
and "Governance meetings", and we quickly closed those efficient "Tactical"
meetings in our hearts. In short: a "Tactical" is a meeting-format, but without
the endless talks and no results. It is designed to get sh*t done, but you have
to give it some time to get used to.

And what about those unlimited holidays? Well, before we could implement it
someone quickly used the Holacracy "governance" format to find an elegant way to
leave for holiday for 6 weeks. The Holacracy governance was somewhat
uncomfortable, but Peter and I liked it. In short: if you want to change
something in the company, you have to schedule a meeting with the roles
involved. A governance meeting follows a strict process, but in essence: you can
propose a change to to the "rules of the game", and if no-one is able to *proof*
that the decision does significant irreversible harm to the company, it will be
approved. You can imagine that this system allowed for a lot of changes, which
helped us adapt to the new realities a growing company encounters on a monthly
basis. So my coworker scheduled a meeting with everyone involved, and just
governed a holiday-request for 6 weeks. And because he just "asked" nicely with
a good reason, everyone wanted to take over his roles and responsibilities
during his absence. Peter and I couldn't object: we couldn't proof that his
holiday would do significant irreversible harm to the company. And also, we
didn't want to stop it: for the first time, we didn't have to think about a HR
situation. 

A new vibe was born within the company. People felt that they could change
things in the company they couldn't change before. For instance, before
holacracy I was in charge of setting the mood: by throwing parties, allowed for
dinners and organizing the occasional team activity. After holacracy, we had our
own Social Events Team, a team with young and very enthusiastic members and our
culture got better and better. They allowed me to join, but soon I wasn't able
to match the awesomeness this new team brought to the table. Yes, a new team was
created, and no manager or owner was needed to initiate it.

And for us? The different teams of the company (or circles, when we use
Holacracy terms) quickly became self sufficient, they implemented the necessary
HR processes matching Holacracy (salary/performance reports,etc) and Peter and I
could take some distance and naturally stopped micromanaging. Which, in turn,
allowed us to do what we do best: use our creativity to create breakthroughs in
our field. I got to do what I love, turned back to my keyboard and created
Python frameworks, refreshed my Kubernetes knowledge, and enjoyed every coworker
in this re-invented company.

And in the end, yes we sold the company. After we consulted a lot of our fellow
coworkers. And we didn't sell because we wanted to get rid of the company, but
because we needed to. A story for another time.

Thank you for reading!

If you want to get into Holacracy, and would like to get some insights, don't
hesitate to get in contact!
