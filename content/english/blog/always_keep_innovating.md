---
title: "Always challenge your employees"
meta_title: ""
description: "A startup is all about change, keep innovating"
date: 2024-04-23T05:00:00Z
image: "/images/blog/innovating.jpeg"
categories: ["Entreprenerdship"]
author: "Michiel Ootjers"
tags: ["cto", "innovation"]
draft: false
---
A startup is all about change. You will attract people who want to be part of
that. I found out that you should always challenge them. Keep innovating.

Having a Tech start-up is hard work. Especially when you build a company without
external investors: you are continuously chasing your customers to pay their
bills, just in time to pay your own employees (and if you are lucky, even your
own management fee). I'm proud that we made it work, and successfully grew our
company to 40 employees and created one of the best adaptive case management
systems in use within the Dutch Government.

Trying to build a sustainable organization takes time and you are lucky when you
have a profitable organization within 10 years. At first you "fake it till you
make it" to get those launching customers. You run from demo to demo, and pull
all-nighters to quickly add those features to satisfy your first customers.
Until the point that things starting to work out and you are able to look into
the future. (...to find out that you created a big vodka-induced codebase which
could quickly be identified as technical debt)

All doom and gloom? No...absolutely not, because if you got this far: YOU WILL
FEEL ALIVE! Because you did it. With maybe 5 or 6 people, you chased your dreams
and it looks like this is actually going to work. Because of the all-nighters
you bonded with you coworkers. Or in my case: after every week of hard work, we
emptied the liquor-cabinet and celebrated until deep into the night. Coworkers
became our friends, families come in for the occasional rooftop BBQ (with a
broken banister, because you work in the cheapest office you could find), and
even our customers loved to come over for a beer. You got to create your own
little world together, and people are starting to pay for what you love to do
most.

After the hangover you get back together on Monday to figure out how to keep the
funds flowing and how to keep adding new features while improving the code
quality at the same time. Because, I'm writing this as a Tech-guy, I was always
looking for time to improve the system, and thankfully, my business partner
understood it's importance and tried to make that happen.

## Be the Apple for the Government
Our first product in 2010 was a mash-up of existing open source applications
with some custom made data entry screens. We made this mashup look like a single
product and made it "sexy" using the skills of a highly talented HTML/CSS guy.
It looked great for the demos, and helped us get new customers. At the same
time, this strategy made sure our product was stable, because we used already
proven applications for storing our data. Our plan worked: this way we could
quickly go to market, and it prevented us from creating too much technical debt
at the same time. We used our customers feedback, and we eventually came to the
conclusion that we had outgrown our mashup and needed something flexible.
Something which also reflected our "Year-plan". Yeah...we had a plan, but no
worries: we could have written it on a post-it.

Our Yearplan:

**In 2012 we:** 
* Have more then 250.000 euro in revenue
* Have more then 5 customers
* Are the Apple for the Government

End.

A case management system absolutely requires you to use more than "only one
click of a mouse button", but we did believe that we could make it simpler and
appealing. We changed our technology (moving away from generating HTML
templates) by choosing a JavaScript framework and a Backend API framework.
Choosing a JavaScript framework was a gamble back then. We chose AngularJS over
ExtJS (React, VueJS, etc were yet something of the future), and continued using
Perl as our Backend language. Our system became a "Web 2.0" application, we
attracted young talent (in both JavaScript and Perl) and our software moved away
from the mashup we used, to a fully grown application. We organised sessions
with our customers to keep them engaged and used their feedback in our new
design. After finishing this process in late 2013, our customers loved this
direction and loved the "new" application.

Things were moving in the right direction, and we didn't have to worry about
our competition much. Until that time our competition was trying to sell their
"document management systems" as a case management system. And most of them
giants dismissed our organisation as a joke, as they all do when a new kid on
the block arrives. But after a couple of years, they finally understood that
they really needed to abandon their old applications for something new, and now
they were struggling to keep up with us.

Well... there was one competitor at that time which gave us the additional drive
we needed to beat them. Hell, even the ball on our
"[Swingball](https://www.decathlon.nl/p/tennispaal-set-turnball-strong-voor-speedball/_/R-p-165192)"
game was named after this competitor. It gave us the time to get to the top 3 of
the market, a comfortable place. And that's where mistakes are made... 

## Maintenance mode

Sure, we didn't have a lot of customers yet, but after the renewal of our
product I believed we were feature complete, enough features for sales to sell
the product to new customers. I believed I could use this time to prepare
the application for "all the users" yet to come. To make sure we didn't have to
throw hardware at it in the same rate as a monkey is eating peanuts. And
although I had a point, I was forgetting a few important things. Besides that a
product is _never_ finished, customers always need more features and the world
is always changing.

As I see it, we went in "maintenance mode". A phase when you focus on your
existing application, and stop building new functionalities. I love improving
existing stuff, but I forgot to look around. By removing innovation from the
equation, I was essentially killing the company. For the guys who joined in the
last couple of years the company became a lot less interesting. We started
loosing fellow coworkers who were always eager to work on "this new kid on the
block". People who brought in the vibe of invincibility, and that we could do
anything as long as long as we put our mind to it. People who are willing to work their
asses of for "the win".

In the next couple of months, or even a year, it felt like we went in some kind
of hibernation. We lost that "invincible" feeling, we got lazy, and even worse:
it was contagious. Even our "tender team" (my business partner and one other
guy) stopped feeling invincible. We stopped being proud of ourselves, and
started copying our competitors. We wrote hundred of pages of project plans to
win new customers, something we didn't do before. We lost sight of who we were,
and so were our customers. Yes, our product got more stable, but that doesn't
matter when you only have a few customers to test drive it. And more
importantly: we couldn't get our customers to pay "more" because we didn't build
new things. Because we were an Open Source product, we relied on customers to pay
for the hours we put into our system. And try to find customers
willing to pay for features they already have...

## Bringing in new vibe

At the time, it was difficult for us to see how we could turn around and get
into more exciting and profitable waters. We did know that our current mode
wasn't working, and that we wanted the old "vibe" back. But at the same time, we
were afraid to change something not knowing if that would end up winning or
loosing the last available tenders. Luckily, things turned around at the moment
a certain person joined the company when we were recruiting for implementation
consultants. This guy, let's call him Bas, was not a success just because he
brought in a great vibe, but he foremost had this unique blind spot: he believed
that you always needed to be straight with everyone, including your customers.
For the first time we had someone who really challenged us: He got us into a
bunch of escalations with customers, because a direct "no" is not always what
they wanted to hear. But more importantly, he also wasn't afraid to point out
problems with the company...or problems with us, the founders. Something we
weren't used to and it started some kind of competition. Maybe we felt that we
needed to be equally bold, and before we knew it, we started to throw away our
current playbook and defined our company again with people like Bas.

Gone were all those boring project plans. We started announcing _our_ way of
doing things. Go somewhere else if you want Waterfall, we do SCRUM. We also
tried to get rid of all fixed price contracts: we can offer you an estimated
price per sprint, and we will let you know what we can do for you in the next
two weeks. Between 2015 en 2017 we started moving from Perl to Python, from
server clusters to Kubernetes, from Angular to ReactJS. And from "maintenance
mode" back to "World Domination". We felt invincible again. We had won almost
all Tenders in 2017, which brought in so much work that I even had to
abandon coding just to calm everyone down, including our consultants: to let
them feel that this tsunami of work was the best we could have ever dreamed of
("Just send me as a project manager if pressure gets to extreme, I will figure
it out").

When looking back, we can't really point our finger on what action really turned
around our company. I'm known to thrive in a big crisis, but this wasn't just a
short crisis. Somehow I believe it truly helps when you make sure you have
people around you who want to take on anything to make a success of something,
and that you have to make sure you give them enough fuel to be willing to do it.

But also:

* A product is never finished. Customers always want more, the world is
changing, laws are changing. Keep that in mind when creating your pricing model.
* This means that you should implement a workflow to build quality features. A
feature race in the beginning is alright, but there won't be much time to get
back to "old failures". Yes, I got the product stable in 2015, but we added a
lot of features after that which could have easiliy undone all the work I did at
that time.
* When you are in the business of creating a product, make sure you generate
recurring revenue. Even if you are able to let your customers pay for new
features. You will need to improve certain aspects of your product in the
future. And your customers don't want to pay for "existing things".
* Hire people who are different than you. It may be easy to find a new "you",
but in the end, you will need all kind of people to tackle all kind of
(cultural) issues.
* A startup is all about change. You will attract people who want to be part of
that. You will have to keep innovating, keep challenging them not only to fuel
_their_ needs, but also to keep it interesting for yourself.

Thanks for reading!

If you think these insights could be useful, and you think I could help you out,
don't hesitate to get in contact!
