---
title: "Hi, I'm Michiel Ootjers!"
meta_title: "About"
description: "this is meta description"
image: "/images/about_michiel_ootjers.png"
draft: false
---

I was born in the 80's and always curious after technology. After my father gave me a book on PC-DOS when I was ten, and bricking his computer afterward (FORMAT was the first command in the book), I've come a long way. First by building a math-game for my cousin in BASIC, and later by installing Red Hat on a borrowed 486. When I was 18, I started my own bussiness and was already building websites in PHP and provided hosting on a server in Amsterdam. A Linux based server with a custom made hosting control panel for my customers.

By joining internet provider XS4ALL, I learned the tricks of the trade. Building their provisioning-system to make sure everyone got their e-mail address, hosting and ADSL signal after entering there information on a website. It was after a few years that I started to miss being a entrepreneur, and worked for different companies to help them translate their success into highly scalable Linux server clusters. This was before I started my main project: building a massive enterprise system for the Dutch local government in Perl, Python and JavaScript which is still in use (nowadays called "xxllnc Zaken").

As a programmer, Cloud Native enthousiast and entrepreneur, I've always looking to upgrade my time. By improving my time management skills and making sure I'm always up to speed on automation. First using Puppet, Chef and Ansible, and later in the cloud with stuff like Helm, Terraform and gitlab pipelines. The introduction of Kubernetes was love on first sight, and since that time I'm helping companies implement a principle called Platform Engineering. To make sure developers have their integrated developer platform for a quick time to market.

In my free time I love enjoying time with my wife, little son and daughter, I love going out with my boat on the water or enjoy time with my friends from Amsterdam: eating food from my BBQ together with a fine glass of wine.
