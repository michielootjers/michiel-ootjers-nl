---
# Banner
banner:
  title: "Your technology partner"
  content: "As a technology enthousiast, I love helping companies to overcome their technological challenges. Moving to the cloud in a role as hands-on engineer, implementing the necessary quality assurance processes or helping your board making the right technological decisions: as a former founder myself I know that a product is never finished and I've got to experience every aspect of a successfully grown startup."
  image: "/images/platform_engineering_front.png"
  button:
    enable: true
    label: "Contact me"
    link: "/contact"

# Features
features:
  - title: "Who am i?"
    image: "/images/services_provided.png"
    content: "I'm Michiel Ootjers, born in the 80s, coding since I was twelve. Founded a succesful startup in which my former employees found me to be an excellent people manager. I've been known to thrive when there is a crisis and always find a way to implement modern solutions (technological or organizational) for company challenges."
    bulletpoints:
      - "Over 20 years of experience in Ops and Development"
      - "Always interested in modern proven technology"
      - "Founder, CTO, Board-member, Lead Developer or Cloud Engineer, I love it all"
      - "My people skills and technological background help teams to respect me"
      - "I believe people will do the best thing when you empower them"
      - "I'm comfortable to work with engineering sizes from"
    button:
      enable: false
      label: "Get Started Now"
      link: "#"

  - title: "My skillset"
    image: "/images/skills.png"
    content: "Bootstrapping a new product, turning your existing monolith to a MicroService architecture, laying the groundwork for your development team by setting up a production grade Kubernetes cluster or just setting up a new engineering team. Choose your battle."
    bulletpoints:
      - "Excellent problem solving skills in either Operations or Development"
      - "Implemented compliance such as ISO27001 or ENSIA"
      - "Building engineering teams (platform and code) using SCRUM or KANBAN"
      - "Database design, Microservice Architecture, DDD, CQRS/Eventsourcing"
      - "Over 15 years of experience in backend languages like Perl, Python, PHP or JS"
      - "Automation is my friend with IaC: Gitlab/Github, Helm, Terraform, etc"
    button:
      enable: true
      label: "Read my LinkedIn profile"
      link: "https://linkedin.com/in/michielootjers"
---
