FROM node:18.16.1

ARG HUGO_ENV="production"
ARG HUGO_VERSION="0.121.2"
ARG GO_VERSION="1.20.5"
ARG NODE_VERSION="18.16.1"
ARG PLATFORM="Linux-64bit"
# ARG PLATFORM="linux-arm64"

WORKDIR /build

RUN apt-get update && apt-get install -y curl \
  && curl -LO "https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/hugo_extended_${HUGO_VERSION}_${PLATFORM}.tar.gz" \
  && tar -xvf hugo_extended_${HUGO_VERSION}_${PLATFORM}.tar.gz \
  && mv hugo /usr/local/bin/ \
  && rm hugo_extended_${HUGO_VERSION}_${PLATFORM}.tar.gz \
  && echo "HUGO ${HUGO_VERSION} INSTALLED" \
  && curl -LO "https://dl.google.com/go/go${GO_VERSION}.${PLATFORM}.tar.gz" \
  && tar -C /usr/local -xzf go${GO_VERSION}.${PLATFORM}.tar.gz \
  && export PATH=$PATH:/usr/local/go/bin \
  && rm go${GO_VERSION}.${PLATFORM}.tar.gz \
  && echo "GO ${GO_VERSION} INSTALLED"

WORKDIR /src
COPY . /src/

ENV PATH /usr/local/go/bin/:$PATH

RUN npm install && npm run project-setup

